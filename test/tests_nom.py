import unittest

from nom import *

class TestNOM(unittest.TestCase):

    
    def test_nomfictionelle(self):

        self.assertEqual(nomfictionelle('abcD','univers1',1,0), 'Heartstriker')
        self.assertEqual(nomfictionelle('abcDe','univers2',1,0), 'Bloody')
        self.assertEqual(nomfictionelle('abcDe','univers3',25,1), 'Warrick')
        
    