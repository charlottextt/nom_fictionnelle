from tkinter import * 

class nom(Frame):
    

    def __init__(self, pencere):
        Frame.__init__(self, pencere)
        self.pencere = pencere
      
        Label(pencere, text="What is your star wars name? ", relief=GROOVE, width=55).place(x=20, y=10)
        Label(pencere, text="FIRST name: ", relief=GROOVE, width=20).place(x=20, y=40)
        self.Ent1 = Entry(pencere, width=30)
        self.Ent1.place(x=180, y=40)

        Label(pencere, text="LAST name: ", relief=GROOVE, width=20).place(x=20, y=70)
        self.Ent2 = Entry(pencere, width=30)
        self.Ent2.place(x=180, y=70)


        Button(pencere, text="Use your FRIST letter of FIRST name", relief=GROOVE, width=55,command=lambda: self.nomfictionelle('univers1',0,1)).place(x=20, y=100)
        Button(pencere, text="Use your FRIST letter of LAST name", relief=GROOVE, width=55,command=lambda: self.nomfictionelle('univers2',0,1)).place(x=20, y=130)
        Button(pencere, text="Use your FRIST letter of LAST name", relief=GROOVE, width=55,command=lambda: self.nomfictionelle('univers3',25,2)).place(x=20, y=160) 
        
        Label(pencere, text="Your name fictionnel:", relief=GROOVE, width=20).place(x=20, y=190)
        self.RESULT1 = Entry(pencere, width=30)
        self.RESULT1.place(x=180, y=190)


    def nomfictionelle(self,nomunivers,n,nom):
        univers={'univers1':{"A":"Heartstriker","B":"Neightfall","C":"Vagabond","D":"Oath","E":"Judge",\
            "F":"Breaker","G":"Mayhem","H":"Spirit","I":"Commander","J":"Marvel","K":"Fire",\
            "L":"Destroyer","M":"Widowmaker","N":"Vengance","O":"Storm","P":"Fiame","Q":"Blade",\
            "R":"Chaos","S":"Giant","T":"Steel","U":"Thunder","V":"Hellhound","W":"Butcher",\
            "X":"Chaos","Y":"Sorrow","Z":"Jammer"},\
            'univers2':{"A":"Bloody","B":"Vulgar","C":"Maddening","D":"Ragged","E":"Sordid",\
            "F":"Mortal","G":"Barbarous","H":"Subbom","I":"Cruel","J":"Bitter","K":"Jagged",\
            "L":"Haunted","M":"Sovereign","N":"Crimson","O":"Mighty","P":"Ferral","Q":"Imperial",\
            "R":"Jagged","S":"Lethal","T":"Ferocious","U":"Intrepid","V":"Somber","W":"Maddening",\
            "X":"Ruthless","Y":"Sacred","Z":"Unforgiving"},\
            'univers3':{"A":"Bane","B":"Sidious","C":"Skywalker","D":"Chewbacca","E":"Felt",\
            "F":"Trooper","G":"PO","H":"Speeder","I":"Dooku","J":"Pit","K":"Vader",\
            "L":"DZ","M":"Palpatine","N":"Grievous","O":"Binks","P":"Soldier","Q":"Kenobi",\
            "R":"Solo","S":"The Huit","T":"Maul","U":"Queen","V":"Windu","W":"Leia",\
            "X":"Monster","Y":"Yoda","Z":"Warrick"}}
        if nom==1:
            name = self.Ent1.get().upper()
        else:
            name = self.Ent2.get().upper()
        name = list(name)
        letter=nom[n]
        self.RESULT1.delete(0, END)
        self.RESULT1.insert(0, univers[nomunivers][letter])
        return univers[nomunivers][letter]


if __name__ == "__main__":
    root = Tk()
    root.title("get nom fictionelle")
    root.geometry("430x270+50+50")
    nom(root).pack(side="top", fill="both")
    root.mainloop()